const tasks = [
    { title: 'Faire les courses', isComplete: false },
    { title: 'Nettoyer la maison', isComplete: true },
    { title: 'Planter le jardin', isComplete: false }
];


/*
    CRÉÉER UNE FONCTION POUR AJOUTER UNE TÂCHE
*/
const addTask = (task) => [...tasks, task];
const newTask = addTask({ title: 'Apprendre ES6', isComplete: false }); // Un nouveau tableau est créé et comprend les éléments indiqués en argument


/*
    CRÉER UNE FONCTION POUR EFFACER UNE TÂCHE
*/

//La fonction prend pour paramètre le tableau souhaité et le titre de la tâche
const removeTask = (newTask, titleTask) => newTask.filter((eachTask) => eachTask.title !== titleTask);//On filtre le tableau newtask, chaque tâche est retournée si son titre est différent du titre indiqué en argument
const updateListe = removeTask(newTask, 'Nettoyer la maison');  //Un nouveau tableau est créé sans les tâches supprimées


/*
    BASCULER L'ÉTAT D'UNE TÂCHE
*/
const toggleTaskStatus = (listTasks, title) => { //On prend en paramètre un tableau et un titre
    return listTasks.map((taskToggle) => { //On retourne un nouveau tableau, on boucle sur chaque tâche
        if(taskToggle.title === title){ //Si le titre de la tâche correspond au titre indiqué en tant que paramètre
            taskToggle.isComplete = !taskToggle.isComplete; //La valeur du statut de la tâche devient la valeur contraire
        }
        return taskToggle; //On retourne la tâche
    });
}

const testToggleTask = toggleTaskStatus(newTask, 'Apprendre ES6'); // On prend en argument le tableau newtask, et la tâche qui a comme titre 'Apprendre ES6'


/*
    AFFICHER LES LISTES DE TÂCHES
*/
const filteredTasks = (listFilteredTasks, state) => { //On predn en paramètre un tableau et un état
    
    const newFilteredTasks = listFilteredTasks.filter((eachFilteredTask) => { //On filtre le tableau et on boucle sur chaque tâche
        //afficher les tâches incomplètes
        if(state === "In progress"){ //Si l'état indiqué en argument est égal à in progress
            if(eachFilteredTask.isComplete === true){ //Si le statut de la tâche est égal à true
                return false; //On renvoie faux 
            } else {
                return true;
            }
        }

        //uniquement les tâches complétées
        else if(state === "Done"){ //Si l'état indiqué en argument est égal à done
            if(eachFilteredTask.isComplete === true){ //Si le statut de la tâche est égal à true
                return true; //On renvoie vrai 
            } else {
                return false;
            }
        }

        //toutes les tâches
        else{
            return true; //On renvoie tout
        }
    })
    return newFilteredTasks; //On retourne le nouveau tableau filtré
}

const showAllFilteredTasks = filteredTasks(newTask);
const showFinishedFilteredTasks = filteredTasks(newTask, 'Done');
const showInProgressFilteredTasks = filteredTasks(newTask, 'In progress');
console.log(showAllFilteredTasks, showFinishedFilteredTasks, showInProgressFilteredTasks);